package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.domain.ErrorStatus;
import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.ErrorStatusModel;
import cl.entel.tde.integration.soa.factory.ErrorMappingFactory;
import cl.entel.tde.integration.soa.repository.ErrorStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ErrorStatusService {

    @Autowired
    private ErrorStatusRepository errorStatusRepository;

    @Autowired
    private ErrorMappingFactory errorMappingFactory;

    public ErrorStatusService() {
    }

    public ErrorStatus getDAO(Long id) throws EntityNotFoundException {
        Optional<ErrorStatus> status = errorStatusRepository.findById(id);
        if(!status.isPresent()){
            throw new EntityNotFoundException("Error Status with ID: " + id + " not found");
        }
        return status.get();
    }

    public List<ErrorStatusModel> findAll(){
        return StreamSupport.stream(this.errorStatusRepository.findAll().spliterator(), false).map( x -> this.errorMappingFactory.map(x) ).collect(Collectors.toList());

    }
}
