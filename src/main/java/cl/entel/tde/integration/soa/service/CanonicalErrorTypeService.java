package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.domain.CanonicalErrorType;
import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.CanonicalErrorTypeModel;
import cl.entel.tde.integration.soa.factory.CanonicalErrorTypeFactory;
import cl.entel.tde.integration.soa.repository.CanonicalErrorTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CanonicalErrorTypeService {

    @Autowired
    private CanonicalErrorTypeFactory canonicalErrorTypeFactory;

    @Autowired
    private CanonicalErrorTypeRepository canonicalErrorTypeRepository;

    public CanonicalErrorTypeService() {
    }

    public CanonicalErrorTypeModel get(Long id) throws EntityNotFoundException{
        CanonicalErrorType type = this.getDAO(id);
        return this.canonicalErrorTypeFactory.map(type);

    }

    public CanonicalErrorType getDAO(Long id) throws EntityNotFoundException{
        Optional<CanonicalErrorType> type = canonicalErrorTypeRepository.findById(id);
        if(!type.isPresent()){
            throw new EntityNotFoundException("Canonical Error Type with id: " + id + " not found");
        }
        return type.get();
    }

    @Cacheable("service.canonical-type.list.all")
    public List<CanonicalErrorTypeModel> list(){
        Iterable<CanonicalErrorType> types = canonicalErrorTypeRepository.findAll();
        return StreamSupport.stream(types.spliterator(), false).map(x -> this.canonicalErrorTypeFactory.map(x)).collect(Collectors.toList());
    }

    public CanonicalErrorTypeModel create(CanonicalErrorTypeModel type){
        CanonicalErrorType dao = this.canonicalErrorTypeFactory.map(type);
        canonicalErrorTypeRepository.save(dao);
        return canonicalErrorTypeFactory.map(dao);
    }

    public void delete(Long id){
        canonicalErrorTypeRepository.deleteById(id);
    }
}
