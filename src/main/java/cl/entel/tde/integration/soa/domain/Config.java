package cl.entel.tde.integration.soa.domain;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "ESB_CONFIG")
public class Config {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "RCD_STATUS")
    private Integer rcdStatus;

    @Column(name = "CREATIONDATE")
    private String creationDate;

    @OneToMany
    @JoinColumn(name = "CONFIG_ID", referencedColumnName = "ID")
    private List<ConfigList> values;

    public Config() {
    }

    public Config(Long id, String name, Integer rcdStatus, String creationDate) {
        this.id = id;
        this.name = name;
        this.rcdStatus = rcdStatus;
        this.creationDate = creationDate;
        this.values = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRcdStatus() {
        return rcdStatus;
    }

    public void setRcdStatus(Integer rcdStatus) {
        this.rcdStatus = rcdStatus;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public List<ConfigList> getValues() {
        return values;
    }

    public void setValues(List<ConfigList> values) {
        this.values = values;
    }
}
