package cl.entel.tde.integration.soa.factory;

import cl.entel.tde.integration.soa.domain.System;
import cl.entel.tde.integration.soa.model.SystemModel;
import org.springframework.stereotype.Component;

@Component
public class SystemFactory {


    public SystemFactory() {
    }


    public SystemModel lazyProviderMap(System system){
        SystemModel provider = new SystemModel(system.getId(),system.getCode(), system.getName(), system.getDescription());
        return provider;
    }

    public SystemModel lazyMap(System system){
        SystemModel provider = new SystemModel(system.getId(),system.getCode(), system.getName(), system.getDescription());
        return provider;
    }
    public SystemModel map(System system){
        SystemModel provider = new SystemModel(system.getId(),system.getCode(), system.getName(), system.getDescription());
        return provider;
    }



}
