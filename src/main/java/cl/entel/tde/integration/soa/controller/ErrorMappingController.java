package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.ErrorMappingModel;
import cl.entel.tde.integration.soa.service.CanonicalErrorService;
import cl.entel.tde.integration.soa.service.ErrorMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/error/mapping")
@CrossOrigin(origins = "http://localhost:3000")
public class ErrorMappingController {

    @Autowired
    private ErrorMappingService errorMappingService;

    @Autowired
    private CanonicalErrorService canonicalErrorService;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<ErrorMappingModel> getMappingById(@PathVariable("id") Long id){
        try{
            ErrorMappingModel mapping = errorMappingService.find(id);
            return new ResponseEntity<>(mapping, HttpStatus.OK);
        } catch (EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/translate", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<ErrorMappingModel> translate(@RequestParam(name = "provider") String provider, @RequestParam(name = "module", required = false)String module, @RequestParam(name = "subModule", required = false)String subModule, @RequestParam(name = "rawCode", required = false)String rawCode){
        try {
            ErrorMappingModel mappings = this.errorMappingService.translate(provider, module, subModule, rawCode);
            return new ResponseEntity<>(mappings, HttpStatus.OK);
        } catch (EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<ErrorMappingModel>> find(@RequestParam(name = "provider", required = false, defaultValue = "*") String provider, @RequestParam(name = "module", required = false, defaultValue = "*")String module, @RequestParam(name = "subModule", required = false, defaultValue = "*")String subModule, @RequestParam(name = "rawCode", required = false, defaultValue = "*")String rawCode){
        List<ErrorMappingModel> mappings = this.errorMappingService.find(provider, module, subModule, rawCode);
        return new ResponseEntity<>(mappings, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<ErrorMappingModel> create(@RequestBody ErrorMappingModel mapping){
        try {
            mapping = this.errorMappingService.create(mapping);
            return new ResponseEntity<>(mapping, HttpStatus.CREATED);
        } catch (EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public ResponseEntity<ErrorMappingModel> update(@PathVariable("id") Long id, @RequestBody ErrorMappingModel mapping){
        try {
            mapping = this.errorMappingService.update(id, mapping);
            return new ResponseEntity<>(mapping, HttpStatus.CREATED);
        } catch (EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}