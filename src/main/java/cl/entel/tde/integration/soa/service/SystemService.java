package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.domain.System;
import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.SystemModel;
import cl.entel.tde.integration.soa.factory.SystemFactory;
import cl.entel.tde.integration.soa.repository.SystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class SystemService {

    @Autowired
    private SystemRepository systemRepository;

    @Autowired
    private SystemFactory systemFactory;

    public SystemService() {
    }

    public List<SystemModel> getAll(){
        return StreamSupport.stream(systemRepository.findAll().spliterator(), false).map(x-> systemFactory.lazyMap(x)).sorted(Comparator.comparing(SystemModel::getCode)).collect(Collectors.toList());
    }

    public SystemModel get(Long id) throws EntityNotFoundException{
        return systemFactory.map(this.getDAO(id));
    }

    public System getDAO(Long id) throws EntityNotFoundException{
        Optional<System> system = this.systemRepository.findById(id);
        if(!system.isPresent()){
            throw new EntityNotFoundException("System with ID: " + id + " not found");
        }
        return system.get();
    }
}
