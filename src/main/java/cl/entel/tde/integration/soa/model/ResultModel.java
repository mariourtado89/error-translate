package cl.entel.tde.integration.soa.model;

public class ResultModel {

    private ResulDescriptionModel result;

    private Object data;

    public ResultModel() {
    }

    public ResultModel(ResulDescriptionModel result, Object data) {
        this.result = result;
        this.data = data;
    }

    public ResulDescriptionModel getResult() {
        return result;
    }

    public void setResult(ResulDescriptionModel result) {
        this.result = result;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
