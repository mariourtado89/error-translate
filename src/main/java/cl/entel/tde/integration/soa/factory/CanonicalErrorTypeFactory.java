package cl.entel.tde.integration.soa.factory;

import cl.entel.tde.integration.soa.domain.CanonicalErrorType;
import cl.entel.tde.integration.soa.model.CanonicalErrorTypeModel;
import org.springframework.stereotype.Component;

@Component
public class CanonicalErrorTypeFactory {

    public CanonicalErrorTypeFactory() {
    }

    public CanonicalErrorTypeModel map(CanonicalErrorType type){
        CanonicalErrorTypeModel model = new CanonicalErrorTypeModel(type.getId(), type.getType(), type.getDescription());
        return model;
    }

    public CanonicalErrorType map(CanonicalErrorTypeModel model){
        CanonicalErrorType type = new CanonicalErrorType(null, model.getType(), model.getDescription());
        return type;
    }
}
