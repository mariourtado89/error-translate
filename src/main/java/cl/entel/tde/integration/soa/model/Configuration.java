package cl.entel.tde.integration.soa.model;


import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
public class Configuration {

    private Long id;

    private String name;

    private Integer rcdStatus;

    private DateTime creationDate;

    private List<ConfigurationValue> values;

    public Configuration() {
        this.values = new ArrayList<>();
    }

    public Configuration(Long id, String name, Integer rcdStatus, DateTime creationDate) {
        this.id = id;
        this.name = name;
        this.rcdStatus = rcdStatus;
        this.creationDate = creationDate;
        this.values = new ArrayList<>();
    }

    public Configuration(Long id, String name, Integer rcdStatus, DateTime creationDate, List<ConfigurationValue> values) {
        this.id = id;
        this.name = name;
        this.rcdStatus = rcdStatus;
        this.creationDate = creationDate;
        this.values = values;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRcdStatus() {
        return rcdStatus;
    }

    public void setRcdStatus(Integer rcdStatus) {
        this.rcdStatus = rcdStatus;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<ConfigurationValue> getValues() {
        return values;
    }

    public void setValues(List<ConfigurationValue> values) {
        this.values = values;
    }
}
