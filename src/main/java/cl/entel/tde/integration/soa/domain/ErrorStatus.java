package cl.entel.tde.integration.soa.domain;

import javax.persistence.*;

@Entity(name = "ErrorStatus")
@Table(name = "ESB_ERROR_STATUS_TYPE")
public class ErrorStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESB_ERROR_STATUS_TYPE_SEQ")
    @SequenceGenerator(sequenceName = "ESB_ERROR_STATUS_TYPE_SEQ", name = "ESB_ERROR_STATUS_TYPE_SEQ")
    private Long id;

    private String name;

    private String description;

    public ErrorStatus() {
    }

    public ErrorStatus(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
