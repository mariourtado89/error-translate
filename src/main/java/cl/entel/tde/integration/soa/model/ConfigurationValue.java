package cl.entel.tde.integration.soa.model;


public class ConfigurationValue {

    private String value;

    private ConfigurationProperty property;

    public ConfigurationValue() {
    }

    public ConfigurationValue(String value, ConfigurationProperty property) {
        this.value = value;
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ConfigurationProperty getProperty() {
        return property;
    }

    public void setProperty(ConfigurationProperty property) {
        this.property = property;
    }
}
