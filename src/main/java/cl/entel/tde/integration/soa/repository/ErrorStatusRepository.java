package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.ErrorStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ErrorStatusRepository extends CrudRepository<ErrorStatus, Long> {

    public ErrorStatus findByName(String name);

}
