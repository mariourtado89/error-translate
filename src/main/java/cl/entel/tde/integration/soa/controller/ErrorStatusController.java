package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.model.ErrorStatusModel;
import cl.entel.tde.integration.soa.service.ErrorStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/error/status")
@CrossOrigin(origins = "http://localhost:3000")
public class ErrorStatusController {

    @Autowired
    private ErrorStatusService errorStatusService;

    public ErrorStatusController() {
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public List<ErrorStatusModel> list(){
        return this.errorStatusService.findAll();
    }
}
