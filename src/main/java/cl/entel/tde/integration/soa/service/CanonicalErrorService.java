package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.domain.CanonicalError;
import cl.entel.tde.integration.soa.domain.CanonicalErrorType;
import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.CanonicalErrorModel;
import cl.entel.tde.integration.soa.factory.CanonicalErrorFactory;
import cl.entel.tde.integration.soa.repository.CanonicalErrorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CanonicalErrorService {


    @Autowired
    private CanonicalErrorTypeService canonicalErrorTypeService;

    @Autowired
    private CanonicalErrorRepository canonicalErrorRepository;

    @Autowired
    private CanonicalErrorFactory canonicalErrorFactory;

    @Cacheable("service.canonical.get.code-type")
    public List<CanonicalErrorModel> get(String code, String type){
        code = code.replace("*", "%");
        type = type.replace("*", "%");
        List<CanonicalError> canonicalErrors = canonicalErrorRepository.findByCodeAndType(code, type);
        return canonicalErrors.stream().map(x -> canonicalErrorFactory.map(x)).collect(Collectors.toList());
    }

    public List<CanonicalErrorModel> getByTypeID(Long findByTypeID){
        List<CanonicalError> canonicalErrors = canonicalErrorRepository.findByTypeID(findByTypeID);
        return canonicalErrors.stream().map(x -> canonicalErrorFactory.map(x)).collect(Collectors.toList());
    }


    @Cacheable("service.canonical.list.all")
    public List<CanonicalErrorModel> get(){
        Iterable<CanonicalError> canonicalErrors = canonicalErrorRepository.findAll();
        return StreamSupport.stream(canonicalErrors.spliterator(), false).map(x -> canonicalErrorFactory.map(x)).collect(Collectors.toList());
    }

    public CanonicalErrorModel get(Long id) throws  EntityNotFoundException{
        return canonicalErrorFactory.map(this.getDAO(id));
    }

    public CanonicalErrorModel create(CanonicalErrorModel canonicalError) throws EntityNotFoundException{
        CanonicalErrorType type = canonicalErrorTypeService.getDAO(canonicalError.getType().getId());
        CanonicalError dao = new CanonicalError(null, canonicalError.getCode(), canonicalError.getDescription(), canonicalError.getPriority(), type);
        canonicalErrorRepository.save(dao);
        return this.canonicalErrorFactory.map(dao);
    }

    public CanonicalError getDAO(Long id) throws EntityNotFoundException{
        Optional<CanonicalError> canonical = this.canonicalErrorRepository.findById(id);
        if (! canonical.isPresent()){
            throw new EntityNotFoundException("Canonical Error with ID: " + id + " not found");
        }
        return canonical.get();
    }

    public void delete(Long id ){
        canonicalErrorRepository.deleteById(id);
    }
}
