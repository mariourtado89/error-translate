package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.ErrorMapping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ErrorMappingRepository extends CrudRepository<ErrorMapping, Long> {


    @Query("select em from ErrorMapping em where em.errorSource.code like ?1 and em.module like ?2 and em.subModule like ?3 and em.rawCode like ?4")
    public List<ErrorMapping> find(String system, String module, String subModule, String rawCode);

    @Query("select em from ErrorMapping em where em.errorSource.code = ?1 and em.module = ?2 and em.subModule = ?3 and em.rawCode = ?4")
    public Optional<ErrorMapping> translate(String system, String module, String subModule, String rawCode);

    @Query("select em from ErrorMapping em where em.errorSource.code = ?1 and em.module = ?2 and em.subModule is null and em.rawCode = ?3")
    public Optional<ErrorMapping> translateModule(String system, String module, String rawCode);

    @Query("select em from ErrorMapping em where em.errorSource.code = ?1 and em.module is null and em.subModule is null and em.rawCode = ?2")
    public Optional<ErrorMapping> translateProvider(String system, String rawCode);

    @Query("select em from ErrorMapping em where em.errorSource.code = ?1 and em.module = ?2 and em.subModule = ?3 and em.rawCode is null")
    public Optional<ErrorMapping> translateGenericSubModule(String system, String module, String subModule);

    @Query("select em from ErrorMapping em where em.errorSource.code = ?1 and em.module = ?2 and em.subModule is null and em.rawCode is null")
    public Optional<ErrorMapping> translateGenericModule(String system, String module);

    @Query("select em from ErrorMapping em where em.errorSource.code = ?1 and em.module is null and em.subModule is null and em.rawCode is null")
    public Optional<ErrorMapping> translateGenericProvider(String system);

}
