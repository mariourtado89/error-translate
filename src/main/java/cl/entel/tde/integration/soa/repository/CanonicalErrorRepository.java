package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.CanonicalError;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CanonicalErrorRepository extends CrudRepository<CanonicalError, Long> {

    public List<CanonicalError> findByCode(String code);

    @Query("select c from CanonicalError c where c.code like ?1 and c.type.type like ?2")
    public List<CanonicalError> findByCodeAndType(String code, String type);

    @Query("select c from CanonicalError c where c.type.id like ?1")
    public List<CanonicalError> findByTypeID(Long typeID);

}
