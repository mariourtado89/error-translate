package cl.entel.tde.integration.soa.domain;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class ConfigListPK implements Serializable {

    @ManyToOne
    @JoinColumn(name = "PROPERTY_ID")
    private ConfigProperty property;

    @ManyToOne
    @JoinColumn(name = "CONFIG_ID")
    private Config config;

    public ConfigListPK() {
    }

    public ConfigListPK(Config config, ConfigProperty property) {
        this.property = property;
        this.config = config;
    }

    public ConfigProperty getProperty() {
        return property;
    }

    public void setProperty(ConfigProperty property) {
        this.property = property;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}

