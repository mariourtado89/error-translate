package cl.entel.tde.integration.soa.factory;

import cl.entel.tde.integration.soa.domain.ErrorMapping;
import cl.entel.tde.integration.soa.domain.ErrorStatus;
import cl.entel.tde.integration.soa.model.CanonicalErrorModel;
import cl.entel.tde.integration.soa.model.ErrorMappingModel;
import cl.entel.tde.integration.soa.model.ErrorStatusModel;
import cl.entel.tde.integration.soa.model.SystemModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ErrorMappingFactory {

    @Autowired
    private SystemFactory systemFactory;

    @Autowired
    private CanonicalErrorFactory canonicalErrorFactory;

    public ErrorMappingFactory() {
    }

    public ErrorMappingModel map (ErrorMapping mapping){
        SystemModel provider = systemFactory.lazyProviderMap(mapping.getErrorSource());
        CanonicalErrorModel canonical = canonicalErrorFactory.map(mapping.getCanonicalError());
        ErrorStatusModel status = this.map(mapping.getStatus());
        ErrorMappingModel model = new ErrorMappingModel(mapping.getId(), provider, mapping.getModule(), mapping.getSubModule(), mapping.getRawCode(), mapping.getRawDescription(), status, canonical);
        return model;
    }

    public ErrorStatusModel map (ErrorStatus status){
        ErrorStatusModel model = new ErrorStatusModel(status.getId(), status.getName(), status.getDescription());
        return model;
    }

}
