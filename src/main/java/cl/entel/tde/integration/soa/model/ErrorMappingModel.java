package cl.entel.tde.integration.soa.model;

public class ErrorMappingModel {

    private Long id;

    private SystemModel errorSource;

    private String module;

    private String subModule;

    private String rawCode;

    private String description;

    private ErrorStatusModel status;

    private CanonicalErrorModel canonical;

    public ErrorMappingModel() {
    }

    public ErrorMappingModel(Long id, SystemModel errorSource, String module, String subModule, String rawCode, String description, ErrorStatusModel status, CanonicalErrorModel canonical) {
        this.id = id;
        this.errorSource = errorSource;
        this.module = module;
        this.subModule = subModule;
        this.rawCode = rawCode;
        this.description = description;
        this.status = status;
        this.canonical = canonical;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SystemModel getErrorSource() {
        return errorSource;
    }

    public void setErrorSource(SystemModel errorSource) {
        this.errorSource = errorSource;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getSubModule() {
        return subModule;
    }

    public void setSubModule(String subModule) {
        this.subModule = subModule;
    }

    public String getRawCode() {
        return rawCode;
    }

    public void setRawCode(String rawCode) {
        this.rawCode = rawCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ErrorStatusModel getStatus() {
        return status;
    }

    public void setStatus(ErrorStatusModel status) {
        this.status = status;
    }

    public CanonicalErrorModel getCanonical() {
        return canonical;
    }

    public void setCanonical(CanonicalErrorModel canonical) {
        this.canonical = canonical;
    }
}
