package cl.entel.tde.integration.soa.aop;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAdviser {

    Logger logger = LoggerFactory.getLogger(LogAdviser.class);

    @AfterThrowing(pointcut = "execution(* cl.entel.tde.soa.integration.service.*.*(..))", throwing="ex")
    public void logReturningError(Exception ex){
        logger.error( ex.getClass().getName() + " -> " + ex.getMessage());
    }
}
