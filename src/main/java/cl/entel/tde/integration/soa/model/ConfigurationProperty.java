package cl.entel.tde.integration.soa.model;

import org.joda.time.DateTime;

public class ConfigurationProperty {

    private Long id;

    private String name;

    private String description;

    private DateTime creationDate;

    public ConfigurationProperty() {
    }

    public ConfigurationProperty(Long id, String name, String description, DateTime creationDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }
}
