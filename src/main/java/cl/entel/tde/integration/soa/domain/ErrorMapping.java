package cl.entel.tde.integration.soa.domain;

import javax.persistence.*;

@Entity(name = "ErrorMapping")
@Table(name = "ESB_ERROR_MAPPING")
public class ErrorMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESB_ERROR_MAPPING_SEQ")
    @SequenceGenerator(sequenceName = "ESB_ERROR_MAPPING_SEQ", name = "ESB_ERROR_MAPPING_SEQ")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ERROR_SOURCE")
    private System errorSource;

    private String module;

    private String subModule;

    private String rawCode;

    private String rawDescription;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CAN_ERR_ID")
    private CanonicalError canonicalError;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "STATUS_ID")
    private ErrorStatus status;

    public ErrorMapping() {
    }

    public ErrorMapping(System errorSource, String module, String subModule, String rawCode, String rawDescription, CanonicalError canonicalError, ErrorStatus status) {
        this.errorSource = errorSource;
        this.module = module;
        this.subModule = subModule;
        this.rawCode = rawCode;
        this.rawDescription = rawDescription;
        this.canonicalError = canonicalError;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public System getErrorSource() {
        return errorSource;
    }

    public void setErrorSource(System errorSource) {
        this.errorSource = errorSource;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getSubModule() {
        return subModule;
    }

    public void setSubModule(String subModule) {
        this.subModule = subModule;
    }

    public String getRawCode() {
        return rawCode;
    }

    public void setRawCode(String rawCode) {
        this.rawCode = rawCode;
    }

    public String getRawDescription() {
        return rawDescription;
    }

    public void setRawDescription(String rawDescription) {
        this.rawDescription = rawDescription;
    }

    public CanonicalError getCanonicalError() {
        return canonicalError;
    }

    public void setCanonicalError(CanonicalError canonicalError) {
        this.canonicalError = canonicalError;
    }

    public ErrorStatus getStatus() {
        return status;
    }

    public void setStatus(ErrorStatus status) {
        this.status = status;
    }
}
