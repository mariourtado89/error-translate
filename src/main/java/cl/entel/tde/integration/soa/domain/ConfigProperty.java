package cl.entel.tde.integration.soa.domain;


import javax.persistence.*;

@Entity
@Table(name = "ESB_CONFIG_PROPERTY")
public class ConfigProperty {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "RCD_STATUS")
    private Integer rcdStatus;

    @Column(name = "CREATIONDATE")
    private String creationDate;

    public ConfigProperty() {
    }

    public ConfigProperty(Long id, String name, Integer rcdStatus, String creationDate) {
        this.id = id;
        this.name = name;
        this.rcdStatus = rcdStatus;
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRcdStatus() {
        return rcdStatus;
    }

    public void setRcdStatus(Integer rcdStatus) {
        this.rcdStatus = rcdStatus;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}
