package cl.entel.tde.integration.soa.model;

public class CanonicalErrorModel {

    private Long id;

    private String code;

    private String description;

    private String priority;

    private CanonicalErrorTypeModel type;

    public CanonicalErrorModel() {
    }

    public CanonicalErrorModel(Long id, String code, String description, String priority, CanonicalErrorTypeModel type) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.priority = priority;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public CanonicalErrorTypeModel getType() {
        return type;
    }

    public void setType(CanonicalErrorTypeModel type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
