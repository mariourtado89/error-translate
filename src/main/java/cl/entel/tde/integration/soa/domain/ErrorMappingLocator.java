package cl.entel.tde.integration.soa.domain;

import javax.persistence.*;

@Entity(name = "ErrorMappingLocator")
@Table(name = "ESB_ERROR_MAPPING_LOCATOR")
public class ErrorMappingLocator {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESB_ERROR_MAPPING_LOCATOR_SEQ")
    @SequenceGenerator(sequenceName = "ESB_ERROR_MAPPING_LOCATOR_SEQ", name = "ESB_ERROR_MAPPING_LOCATOR_SEQ")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ERROR_SOURCE")
    private System errorSource;

    private String module;

    private String subModule;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CONFIG_ID")
    private Config config;

    public ErrorMappingLocator(System errorSource, String module, String subModule, Config config) {
        this.errorSource = errorSource;
        this.module = module;
        this.subModule = subModule;
        this.config = config;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public System getErrorSource() {
        return errorSource;
    }

    public void setErrorSource(System errorSource) {
        this.errorSource = errorSource;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getSubModule() {
        return subModule;
    }

    public void setSubModule(String subModule) {
        this.subModule = subModule;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
