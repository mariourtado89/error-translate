package cl.entel.tde.integration.soa.controller;

import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.CanonicalErrorModel;
import cl.entel.tde.integration.soa.model.CanonicalErrorTypeModel;
import cl.entel.tde.integration.soa.service.CanonicalErrorService;
import cl.entel.tde.integration.soa.service.CanonicalErrorTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/error/canonical")
@CrossOrigin(origins = "http://localhost:3000")
public class CanonicalErrorController {

    @Autowired
    private CanonicalErrorTypeService canonicalErrorTypeService;

    @Autowired
    private CanonicalErrorService canonicalErrorService;

    public CanonicalErrorController() {
    }

    @RequestMapping(value = "/type/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<CanonicalErrorTypeModel> getType(@PathVariable("id") Long id){
        try{
            CanonicalErrorTypeModel type = canonicalErrorTypeService.get(id);
            return new ResponseEntity<>(type, HttpStatus.OK);
        } catch (EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/type", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<CanonicalErrorTypeModel>> listType(){
        List<CanonicalErrorTypeModel> types = canonicalErrorTypeService.list();
        return new ResponseEntity<>(types, HttpStatus.OK);
    }

    @RequestMapping(value = "/type", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<CanonicalErrorTypeModel> createType(@RequestBody CanonicalErrorTypeModel type){
        type = canonicalErrorTypeService.create(type);
        return new ResponseEntity<>(type, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/type/{id}", method = RequestMethod.DELETE)
    public void deleteType(@PathVariable("id")  Long id){
        canonicalErrorTypeService.delete(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<CanonicalErrorModel> get(@PathVariable("id") Long id){
        try{
            CanonicalErrorModel canonical = canonicalErrorService.get(id);
            return new ResponseEntity<>(canonical, HttpStatus.OK);
        } catch ( EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/type/{id}/canonicals", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<CanonicalErrorModel>> find(@PathVariable("id") Long id){
        List<CanonicalErrorModel> canonicals = canonicalErrorService.getByTypeID(id);
        return new ResponseEntity<>(canonicals, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<CanonicalErrorModel>> find(@RequestParam(value="code", defaultValue = "*") String code, @RequestParam(value = "type", defaultValue = "*") String type){
        List<CanonicalErrorModel> canonicals = canonicalErrorService.get(code, type);
        return new ResponseEntity<>(canonicals, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<CanonicalErrorModel> create(@RequestBody CanonicalErrorModel canonical){
        try{
            canonical = canonicalErrorService.create(canonical);
            return new ResponseEntity<>(canonical, HttpStatus.CREATED);
        } catch (EntityNotFoundException e ){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id){
        canonicalErrorService.delete(id);
    }
}
