package cl.entel.tde.integration.soa.domain;

import javax.persistence.*;

@Entity(name = "CanonicalError")
@Table(name = "ESB_CANONICAL_ERROR")
public class CanonicalError {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESB_CANONICAL_ERROR_SEQ")
    @SequenceGenerator(sequenceName = "ESB_CANONICAL_ERROR_SEQ", name = "ESB_CANONICAL_ERROR_SEQ")
    private Long id;

    private String code;

    private String description;

    private String priority;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_ID")
    private CanonicalErrorType type;

    public CanonicalError() {
    }

    public CanonicalError(Long id, String code, String description, String priority, CanonicalErrorType type) {
        this.id = id;
        this.code = code;
        this.description = description;
        this.priority = priority;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public CanonicalErrorType getType() {
        return type;
    }

    public void setType(CanonicalErrorType type) {
        this.type = type;
    }
}
