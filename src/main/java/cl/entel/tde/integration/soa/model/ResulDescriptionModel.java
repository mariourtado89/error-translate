package cl.entel.tde.integration.soa.model;

public class ResulDescriptionModel {

    private String code;

    private String description;

    public ResulDescriptionModel() {
    }

    public ResulDescriptionModel(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
