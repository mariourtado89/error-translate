package cl.entel.tde.integration.soa.factory;

import cl.entel.tde.integration.soa.domain.CanonicalError;
import cl.entel.tde.integration.soa.domain.CanonicalErrorType;
import cl.entel.tde.integration.soa.model.CanonicalErrorModel;
import cl.entel.tde.integration.soa.model.CanonicalErrorTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CanonicalErrorFactory {

    @Autowired
    private CanonicalErrorTypeFactory canonicalErrorTypeFactory;

    public CanonicalErrorFactory() {
    }

    public CanonicalErrorModel map(CanonicalError canonical){
        CanonicalErrorTypeModel typeModel = canonicalErrorTypeFactory.map(canonical.getType());
        CanonicalErrorModel model = new CanonicalErrorModel(canonical.getId(), canonical.getCode(), canonical.getDescription(), canonical.getPriority(), typeModel);
        return model;
    }

    public CanonicalError map(CanonicalErrorModel model){
        CanonicalErrorType type = canonicalErrorTypeFactory.map(model.getType());
        CanonicalError canonical = new CanonicalError(null, model.getCode(), model.getDescription(), model.getPriority(), type);
        return canonical;
    }
}
