package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.System;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SystemRepository extends CrudRepository<System, Long> {

    public System findOneByCode(String code);



}
