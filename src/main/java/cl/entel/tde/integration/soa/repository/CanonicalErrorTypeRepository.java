package cl.entel.tde.integration.soa.repository;

import cl.entel.tde.integration.soa.domain.CanonicalErrorType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CanonicalErrorTypeRepository extends CrudRepository<CanonicalErrorType, Long> {
}
