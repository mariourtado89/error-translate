package cl.entel.tde.integration.soa.service;

import cl.entel.tde.integration.soa.domain.CanonicalError;
import cl.entel.tde.integration.soa.domain.ErrorMapping;
import cl.entel.tde.integration.soa.domain.ErrorStatus;
import cl.entel.tde.integration.soa.domain.System;
import cl.entel.tde.integration.soa.exception.EntityNotFoundException;
import cl.entel.tde.integration.soa.model.ErrorMappingModel;
import cl.entel.tde.integration.soa.factory.ErrorMappingFactory;
import cl.entel.tde.integration.soa.repository.CanonicalErrorRepository;
import cl.entel.tde.integration.soa.repository.ErrorMappingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ErrorMappingService {

    @Autowired
    private ErrorMappingRepository errorMappingRepository;

    @Autowired
    private SystemService systemService;

    @Autowired
    private CanonicalErrorRepository canonicalErrorRepository;

    @Autowired
    private CanonicalErrorService canonicalErrorService;

    @Autowired
    private ErrorMappingFactory errorMappingFactory;

    @Autowired
    private ErrorStatusService errorStatusService;

    public ErrorMappingService() {
    }

    public ErrorMappingModel find(Long id) throws  EntityNotFoundException{
        Optional<ErrorMapping> errorMapping = this.errorMappingRepository.findById(id);
        if (!errorMapping.isPresent()){
            throw new EntityNotFoundException("Error Mapping with ID: " + id + " not found");
        }
        return errorMappingFactory.map(errorMapping.get());
    }

    public List<ErrorMappingModel> find(String provider, String module, String subModule, String rawCode){
        provider = provider.replace("*", "%");
        module = module.replace("*", "%");
        subModule = subModule.replace("*", "%");
        rawCode = rawCode.replace("*", "%");
        List<ErrorMapping> mappins = this.errorMappingRepository.find(provider, module, subModule, rawCode);
        return mappins.stream().map(x-> this.errorMappingFactory.map(x)).collect(Collectors.toList());
    }

    public ErrorMappingModel translate (String provider, String module, String subModule, String rawCode)throws  EntityNotFoundException{
        Optional<ErrorMapping> mappings =  this.errorMappingRepository.translate(provider, module, subModule, rawCode);
        if (!mappings.isPresent()){
            return this.translateModule(provider, module, subModule, rawCode);
        }
        return errorMappingFactory.map(mappings.get());
    }

    public ErrorMappingModel translateModule (String provider, String module, String subModule, String rawCode) throws  EntityNotFoundException{
        Optional<ErrorMapping>mappings =  this.errorMappingRepository.translateModule(provider, module,rawCode);
        if (!mappings.isPresent()){
            return this.findProvider(provider, module, subModule, rawCode);
        }
        return errorMappingFactory.map(mappings.get());

    }

    public ErrorMappingModel findProvider (String provider, String module, String subModule, String rawCode) throws  EntityNotFoundException{
        Optional<ErrorMapping> mappings=  this.errorMappingRepository.translateProvider(provider, rawCode);
        if (!mappings.isPresent()){
            this.translateGenerigSubModule(provider, module, subModule, rawCode);
        }
        return errorMappingFactory.map(mappings.get());
    }

    public ErrorMappingModel translateGenerigSubModule (String provider, String module, String subModule, String rawCode) throws  EntityNotFoundException{
        Optional<ErrorMapping>mappings =  this.errorMappingRepository.translateGenericSubModule(provider, module, subModule);
        if (!mappings.isPresent()){
            this.translateGenerigModule(provider, module,subModule, rawCode);
        }
        return errorMappingFactory.map(mappings.get());
    }

    public ErrorMappingModel translateGenerigModule (String provider, String module, String subModule, String rawCode) throws  EntityNotFoundException{
        Optional<ErrorMapping> mappings =  this.errorMappingRepository.translateGenericModule(provider, module);
        if (!mappings.isPresent()){
            this.translateGenerigProvider(provider, module, subModule, rawCode);
        }
        return errorMappingFactory.map(mappings.get());
    }

    public ErrorMappingModel translateGenerigProvider (String provider, String module, String subModule, String rawCode) throws  EntityNotFoundException {
        Optional<ErrorMapping> mappings =  this.errorMappingRepository.translateGenericProvider(provider);
        return errorMappingFactory.map(mappings.get());
    }

    public void delete(Long id){
        this.errorMappingRepository.deleteById(id);
    }

    public ErrorMappingModel create(ErrorMappingModel mapping ) throws EntityNotFoundException {
        System provider = this.systemService.getDAO(mapping.getErrorSource().getId());
        CanonicalError canonicalError = this.canonicalErrorService.getDAO(mapping.getCanonical().getId());
        ErrorStatus errorStatus = this.errorStatusService.getDAO(mapping.getStatus().getId());
        ErrorMapping errorMapping = new ErrorMapping(provider, mapping.getModule(), mapping.getSubModule(), mapping.getRawCode(), mapping.getDescription(), canonicalError, errorStatus);
        this.errorMappingRepository.save(errorMapping);
        return this.errorMappingFactory.map(errorMapping);
    }

    public ErrorMappingModel update(Long id, ErrorMappingModel mapping ) throws EntityNotFoundException {
        ErrorMapping errorMapping = this.errorMappingRepository.findById(id).get();
        System provider = this.systemService.getDAO(mapping.getErrorSource().getId());
        errorMapping.setErrorSource(provider);
        CanonicalError canonicalError = this.canonicalErrorService.getDAO(mapping.getCanonical().getId());
        errorMapping.setCanonicalError(canonicalError);
        ErrorStatus errorStatus = this.errorStatusService.getDAO(mapping.getStatus().getId());
        errorMapping.setStatus(errorStatus);
        errorMapping.setModule(mapping.getModule());
        errorMapping.setSubModule(mapping.getSubModule());
        errorMapping.setRawCode(mapping.getRawCode());
        errorMapping.setRawDescription(mapping.getDescription());
        this.errorMappingRepository.save(errorMapping);
        return this.errorMappingFactory.map(errorMapping);
    }

}