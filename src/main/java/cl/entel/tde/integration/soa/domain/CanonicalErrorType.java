package cl.entel.tde.integration.soa.domain;

import javax.persistence.*;

@Entity(name = "CanonicalErrorType")
@Table(name = "ESB_CANONICAL_ERROR_TYPE")
public class CanonicalErrorType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESB_CANONICAL_ERROR_TYPE_SEQ")
    @SequenceGenerator(sequenceName = "ESB_CANONICAL_ERROR_TYPE_SEQ", name = "ESB_CANONICAL_ERROR_TYPE_SEQ")
    private Long id;

    private String type;

    private String description;

    public CanonicalErrorType() {
    }

    public CanonicalErrorType(Long id, String type, String description) {
        this.id = id;
        this.type = type;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
