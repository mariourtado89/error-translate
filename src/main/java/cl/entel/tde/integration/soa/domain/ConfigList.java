package cl.entel.tde.integration.soa.domain;

import javax.persistence.*;

@Entity
@Table(name="ESB_CONFIG_LIST")
public class ConfigList {

    @EmbeddedId
    private ConfigListPK configListPK;

    @Column(name = "VALUE")
    private String value;

    public ConfigList() {
    }

    public ConfigList(ConfigListPK configListPK, String value) {
        this.configListPK = configListPK;
        this.value = value;
    }

    public ConfigListPK getConfigListPK() {
        return configListPK;
    }

    public void setConfigListPK(ConfigListPK configListPK) {
        this.configListPK = configListPK;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
